import Vue from "vue";
import App from "./App.vue";
import router from "./router";
var Datastore = require("nedb");
var desserts = new Datastore({
  filename: "./database/desserts.db",
  autoload: true
});
var settings = new Datastore({
  filename: "./database/settings.db",
  autoload: true
});
var data = new Datastore({
  filename: "./database/data.db",
  autoload: true
});
var folders = new Datastore({
  filename: "./database/folders.db",
  autoload: true
});

// CHECK IF WE'RE IN BUILD ELECTRON OR SERVE
window.console.log(process.cwd());
window.console.log(__filename + "\n" + __dirname);
if (process.cwd() === "/") {
  //we're in build
  const new_dir = require("path").normalize(
    __dirname + "../../../../../../../"
  );
  window.console.log(`Starting directory: ${process.cwd()}`);
  try {
    process.chdir(new_dir);
    window.console.log(`New directory: ${process.cwd()}`);
  } catch (err) {
    window.console.error(`chdir: ${err}`);
  }
}
if(process.cwd()=="/Users"){
  window.console.log('changing directories again');
  process.chdir("./aaronfriedman/desktop/project/cookbook");
  window.console.log(`New directory: ${process.cwd()}`);

}



Vue.prototype.$db = { desserts, settings, data, folders };
// contains data such as categories, so we don't have to use nedb everytime we want to find it, and should update accordingly
Vue.prototype.$datum = { workspace: "All" };

Vue.config.productionTip = false;

new Vue({
  router,
  render: h => h(App)
}).$mount("#app");
