import Vue from "vue";
import Router from "vue-router";
import Dessert from "./views/Dessert.vue";
import DessertComponent from "./views/DessertComponent.vue";
import SearchDisplay from "./views/SearchDisplay.vue";
import CreateDessert from "./views/CreateDessert.vue";
import Recipe from "./views/Recipe.vue";
import BrowsingDesserts from "./views/BrowsingDesserts.vue";
import EditDessert from "./views/EditDessert.vue";
import Settings from "./views/Settings.vue";
import Folders from "./views/Folders.vue";


Vue.use(Router);

export default new Router({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [
    {
      path: "/dessert",
      name: "dessert",
      component: Dessert,
      props: (route) => ({ query: route.query.stuff }) 

    },
    {
      path: "/dessertComponent",
      name: "DessertComponent",
      component: DessertComponent,
      props: (route) => ({ query: route.query.componenttuff })
    },
    { path: '/search', 
      component: SearchDisplay, 
      props: (route) => ({ query: route.query.q }) 
    },
    {
      path: "/category/:categoryName",
      component: SearchDisplay
    },
    {
      path: "/createDessert",
      name: "CreateDessert",
      component: CreateDessert,
    },
    {
        path: "/recipe",
        name: "Recipe",
        component: Recipe,
        props: (route) => ({ query: route.query.recipestuff }) 
  
      }, 
      {
        path: "/",
        name: "BrowsingDesserts",
        component: BrowsingDesserts,
        props: (route) => ({ query: route.query.info }) 
  
      },
      {
        path: "/edit",
        name: "EditDessert",
        component: EditDessert,
        props: (route) => ({ query: route.query.edit }) 
  
      },
      {
        path: "/settings",
        name: "Settings",
        component: Settings,
  
      },
      {
        path: "/folders",
        name: "Folders",
        component: Folders,
  
      },

  ]
});
