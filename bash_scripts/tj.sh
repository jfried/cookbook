#!/bin/bash
#input="temp.txt"
count=0
instructions="\"instructions\":\""
ingredients="\"ingredients\": ["
input="$1"
#input='../recipes_info/Sablea vanille kipfer.txt'
while IFS= read -r line
do
  count=$((count+1))
if (( count == 3 ))
then
	foo=$line
	name=""
	extra=0
	extra_info=""
	add=false
	for (( i=0; i<${#foo}; i++ )); do
		char=${foo:$i:1}
		if ( $add ); then
			name="$name""$char"
		fi
		if (( $extra > 1)); then
			extra_info="$extra_info""$char"		
		fi
		if [[ $char = $'\t' || $char = $'\n' ]]; then
			extra=$((extra+1))
			if $add; then
				add=false
				name=${name:0:${#name}-2}
			else
				add=true
			fi
		fi
			
	done 
fi
#get ingredients
if (( count > 5 && count <24 )); then
	#echo "$line  $count"
# check if string contains at least two latin letters
	if [[ $line =~ [AEIOUaeiou] ]]; then
		# get name of dessert
		foo=$line
		char=${foo:0:1}
		ing=""
		quant=""
		if [[ $char != " " ]]; then
                	ing+=$char		                
                fi
		if [[ $char = $'\t' ]];then
			continue
		fi
		add_ins=true
		for (( i=1; i<${#foo}; i++ )); do
			char=${foo:$i:1}
			if [[ $char != $'\t' && $add_ins = true ]]; then
                        	ing+=$char
			elif [[ "$add_ins" = "false" && $char = $'\t' ]]; then
				break
			elif [[ $char = $'\t' ]]; then
				add_ins=false
			elif [[ "$add_ins" = "false" ]]; then
				quant+=$char
			#else
				#echo "UH OH THERE'S A PROBLEM"	
			fi	
		done
		# ing and quant are calculated add ing and quant to JSON format
		info="[\"$ing\",\"$quant\"]"
		if (( count == 6 )); then
			ingredients+=" $info"
		else
			ingredients+=", $info"
		fi
	#else
		#echo "lmao $count $line"
	fi
fi
if (( count>27 )); then
	char=${foo:0:1}
	if [[ $char = $'\n' ]];then
		continue
	fi
	instructions+="$line""\\n"
fi
done < "$input"
name="\"name\":\"$name\""
extra_info="\"extra_info\":\"$extra_info\""
ingredients+="]"
instructions=${instructions:0:${#instructions}-2}
instructions+="\""
json_string="{$name, $extra_info, $ingredients, $instructions}"
echo "$json_string"
